from data_ops import connect_to_mongo, runs_to_sparse
from config import DB_HOST, DB_NAME, DB_PORT, PRUNED_KEYS
import random

def run_encoding_test():
    cli = connect_to_mongo(DB_PORT, DB_HOST)
    my_cols = [c for c in cli[DB_NAME].collection_names() 
               if (cli[DB_NAME][c].find().count() < 50000 and cli[DB_NAME][c].find().count() > 500)]

    # we choose 3 reasonably-sized collections randomly
    my_runs = random.sample(my_cols, 3)

    # we load encoded sparse data
    data, encoder = runs_to_sparse(cli, my_runs)

    # we load raw data directly from db
    data_raws = [list(cli[DB_NAME][r].find()) for r in my_runs]

    # decode from sparse matrix to list of dicts
    D = []
    for M in data:
        data_decoded = []
        for d in M:
            ob = encoder.decode_event(d)
            # remove zero-valued
            data_decoded.append({k:ob[k] for k in ob if ob[k]})
        D.append(data_decoded)
    try:
        for t in range(len(D)):
            raw_dicts = data_raws[t]
            rec_dicts = D[t]

            # test 1: check that resulting lists have equal sizes
            assert len(raw_dicts) == len(rec_dicts)

            for i in range(len(raw_dicts)):
                raw_d = raw_dicts[i]
                rec_d = rec_dicts[i]
                # test 2: check that nonzero values of all dicts match     
                for k in rec_d:
                    assert rec_d[k] == raw_d[k]

        print 'Encoding tests passed'
        return 1
    except Exception as e:
        print e
        print 'Failed encoding test'
        return 0
