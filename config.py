# MongoDB
# DB_HOST = '127.0.0.1'
DB_HOST = '10.128.124.12'
DB_PORT = 27017

# Database with the events
DB_NAME = 'lhcb'
# Deprecated
# ENC_DB_NAME = 'lhcb_keys'
# Database for storing the comparison results
RES_DB_NAME = 'xgb_results_v2'
TIME_WINDOW_COMPARISON_DB_NAME = 'time_window'
TIME_WINDOW_COMPARISON_COLLECTION = 'results'

GROUP_INTERVAL = 3 #MINUTES, RUN PARTITIONING GRANULARITY
MIN_SIZE = 50 #EVENTS, MINIMUM COLLECTION(RUN) SIZE SUITABLE FOR PROCESSING
# Sample sizes for run pairs comparison
SAMPLE_SIZES = (50000, 120000)
# MongoDB keys not used for comparing runs. All other keys are used!
PRUNED_KEYS = ['_id', 'eventNumber', 'gpsTime', 'runNumber']

# mass_process.py
# ALL - compare all possible run pairs - O(n^2)
# PAIR - compare the consequent* run pairs. *according to MongoDB
MODE = 'PAIR'
# Whether show a progress bar during run loading
PROG = False
XGB_PAR = {'max_depth':4, 'eta':1, 'silent':1, 'objective':'binary:logistic',
           'nthreads':4, 'silent':True, 'verbose_eval':False}
XGB_CV_PARAMETERS = {'num_boost_round':5, 'nfold':3}
