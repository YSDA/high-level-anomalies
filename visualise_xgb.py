import plotly.plotly as py
import plotly.graph_objs as go
import numpy as np

from config import DB_HOST, DB_PORT, RES_DB_NAME
from data_ops import connect_to_mongo

cli = connect_to_mongo(DB_PORT, DB_HOST)
db = cli[RES_DB_NAME]
R = {}

for col in db.collection_names():
    R[col] = list(db[col].find())

py.sign_in('aphex', 'jwyjv4h2vr')

#traces = []
#nbars = max([len(R[k]) for k in R])
i = 1

trace1 = go.Bar(
    x=np.array([k for k in R]),
    y=np.array([R[k][i]['results'][-1] for k in R])[:, 0],
    name='test_auc'
)

trace2 = go.Bar(
    x=np.array([k for k in R]),
    y=np.array([R[k][i]['results'][-1] for k in R])[:, 2],
    name='train_auc'
)

data = [trace1, trace2]
layout = go.Layout(
    barmode='group',
    title='AUC report, sample size = {}'.format(R[k][i]['sample_size']),
    xaxis=dict(
        title='pair_id',
        titlefont=dict(family='Courier New, monospace', size=18,color='#7f7f7f')),

    yaxis=dict(
        title='AUC score',
        titlefont=dict(family='Courier New, monospace',size=18,color='#7f7f7f'
        )))

fig = go.Figure(data=data, layout=layout)
print 'Please visit https://plot.ly/~aphex/12/auc-report-sample-size-19072/'
