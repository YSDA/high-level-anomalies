import json
import requests
import numpy as np

from config import (
    DB_PORT, DB_HOST, DB_NAME, RES_DB_NAME,
    XGB_PAR, MIN_SIZE, SAMPLE_SIZES,
    TIME_WINDOW_COMPARISON_COLLECTION, TIME_WINDOW_COMPARISON_DB_NAME)
from analysis import compare_runs_xgb, get_time_window_auc_in_run
from operator import itemgetter
from collections import defaultdict
from itertools import combinations, chain
from data_ops import connect_to_mongo, get_runs_with_data
import argparse


def get_run_pairs(cli):
    db = cli[DB_NAME]
    runs = list(sorted([int(c.split('_')[1]) for c in db.collection_names()
                if db[c].find().count() > MIN_SIZE]))
    return list(zip(runs, runs[1:]))


def get_all_run_pairs(cli):
    raise RuntimeError("Calling a deprecated function")
    P = []
    db = cli[DB_NAME]
    cls = [c for c in db.collection_names() if db[c].find().count() > MIN_SIZE]
    
    for i, j in combinations(cls, 2):
        P.append((int(i.split('_')[1]), int(j.split('_')[1])))
    return P


def get_run_pairs_from_problems(problems_file):
    with open(problems_file, "r") as problems_io:
        problems = json.load(problems_io)
    pairs = []
    for problem_and_runs in problems:
        runs = list(sorted(chain(*problem_and_runs[1:])))
        pairs.extend(zip(runs, runs[1:]))
    return pairs


def process_by_time_window(cli):
    runs = get_runs_with_data(cli, MIN_SIZE)
    collection = cli[TIME_WINDOW_COMPARISON_DB_NAME][TIME_WINDOW_COMPARISON_COLLECTION]
    for run in runs:
        if collection.find({'run_number': run}).count() > 0:
            continue
        scores = get_time_window_auc_in_run(run, cli)
        if len(scores) > 0:
            collection.insert(scores)


# <low_quality_mode>
def process_run_pairs(cli, pairs):
    sizes = SAMPLE_SIZES
    done = cli[RES_DB_NAME].collection_names()
    print('{} pairs already processed'.format(len(done)))
    TBD = []

    def check_run(run_id):
        return cli[DB_NAME]["run_%d" % run_id].count() > MIN_SIZE
        
    for pair in pairs:
        pnam = str(pair[0]) + '_' + str(pair[1])
        if pnam not in done and all(map(check_run, pair)):
            TBD.append((pair, pnam))

    print('{} pairs to process'.format(len(TBD)))
    print('Boosting with the following parameters: {}'.format(XGB_PAR))
    for i, p in enumerate(TBD):
        pair, col = p
        print('Boosting {}/{}, pair ID: {}'.format(i+1, len(TBD), col))
        r = compare_runs_xgb(pair, cli, sizes, XGB_PAR)
        cli[RES_DB_NAME][col].insert(r, check_keys=False)
# </low_quality_mode>

    
def mass_process():
    parser = argparse.ArgumentParser(
        description="Compares run pairs, writes the results to"
        " MongoDB")
    parser.add_argument("-m", "--mode", type=str, required=True)
    parser.add_argument("--problems-file", type=str)
    args = parser.parse_args()
    pairs = None
    if args.mode == 'all':
        pairs = get_all_run_pairs(cli)
    elif args.mode == 'pairs':
        pairs = get_run_pairs(cli)
    elif args.mode == 'problems':
        pairs = get_run_pairs_from_problems(args.problems_file)
    elif args.mode == 'window':
        pass
    else:
        raise ValueError(("Unknown MODE %s specified in the config file.\n"
                          "Valid modes are: pair, all, problems, window") % MODE)
    cli = connect_to_mongo(DB_PORT, DB_HOST)
    if pairs:
        process_run_pairs(cli, pairs)
    else:
        process_by_time_window(cli)


if __name__ == "__main__":
    mass_process()
