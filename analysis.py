from data_ops import runs_to_sparse, run_to_sparse_partitions, get_key_by_datetime
from datetime import datetime, timedelta
from scipy.sparse import vstack 
import xgboost as xgb
import numpy as np
import sklearn
import random
from config import XGB_PAR, SAMPLE_SIZES, XGB_CV_PARAMETERS, GROUP_INTERVAL, MIN_SIZE
import gc

def compare_runs_xgb(pair, cli, sample_sizes=SAMPLE_SIZES, parameters=XGB_PAR):
    """
    Uses XGBoost to compare a pair of runs.
    Args:
       pair: a tuple with two run numbers
       cli: a MongoDB Client instance
       sample_size: a list of sample sizes to try
       parameters: XGBoost parameters dict
    Returns:
       A dict with the comparison results
    """
    gc.collect()
    
    rounds = parameters['nrounds']
    folds = parameters['nfolds']

    run_one, run_two = pair
    D, _ = runs_to_sparse(cli, pair)
    data_one, data_two = D
    
    data = vstack((data_one, data_two), format="csc")

    labels = np.concatenate((np.zeros(data_one.shape[0]),
                             np.ones(data_two.shape[0])))
    
    data, labels = sklearn.utils.shuffle(data, labels, random_state=34, replace=True)
    results = []
    for sample_size in sorted(sample_sizes):
        actual_sample_size = min(sample_size, data.shape[0])
        idx = np.random.choice(np.arange(labels.shape[0]), actual_sample_size, replace=False)
        dtrain = xgb.DMatrix(data[idx], label=labels[idx], silent=True)
        bst = xgb.cv(bst_param, dtrain, num_boost_round=rounds, silent=True,
                     nfold=folds, metrics={'auc'}, seed=0, as_pandas=False)
        rez = [list(t) for t in bst]
        R = {'parameters':parameters, 'results':rez, 'sample_size':actual_sample_size,
             'timestamp':datetime.now(), 'run_stats':
             {'run_one_num': str(run_one), 'run_one_size': data_one.shape[0],
              'run_two_num': str(run_two), 'run_two_size': data_two.shape[0]}}     
        results.append(R)
        if actual_sample_size < sample_size:
            break

    return results

# <low_quality_mode>
# TODO(kazeevn) remove the code duplication
def get_time_window_auc_in_run(run_number, cli, window_size=3):
    dataframes, timestamps = run_to_sparse_partitions(cli, run_number, window_size)
    run_start = min(timestamps)
    run_end = max(timestamps)
    parameters = XGB_PAR
    border_timestamps = list(sorted(dataframes.keys()))
    pairs = list(zip(border_timestamps, border_timestamps[1:]))
    
    results = []
    for windows in pairs:
        data_one, data_two = dataframes[windows[0]], dataframes[windows[1]]
        if min(data_one.shape[0], data_two.shape[0]) < MIN_SIZE:
            continue
        data = vstack((data_one, data_two), format="csc")
        labels = np.concatenate((np.zeros(data_one.shape[0]),
                                 np.ones(data_two.shape[0])))
        data, labels = sklearn.utils.shuffle(data, labels, random_state=34, replace=True)
        dtrain = xgb.DMatrix(data, label=labels, silent=True)
        xgb_cv_results = xgb.cv(parameters, dtrain, metrics={'auc'}, seed=0,
                                as_pandas=False, **XGB_CV_PARAMETERS)
        window_results = {
            'parameters':parameters, 'results':list(xgb_cv_results[-1]), 'timestamp':datetime.now(),
            'border_timestamp':windows[1], 'run_number':run_number,
            'windows': [
                {'sample_size': data_one.shape[0],
                 'start': max(run_start, windows[0]),
                 'end': min(run_end, windows[1])},
                {'sample_size': data_two.shape[0],
                 'start': max(run_start, windows[1]),
                 'end': min(run_end, get_key_by_datetime(
                     windows[1] + timedelta(minutes=window_size), GROUP_INTERVAL))}
            ]}
        results.append(window_results)
    return results
# </low_quality_mode>
