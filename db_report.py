from data_ops import connect_to_mongo
from config import DB_HOST, DB_PORT, RES_DB_NAME, DB_NAME, MIN_SIZE

cli = connect_to_mongo(DB_PORT, DB_HOST)

for db in cli.database_names():
        print 25*'='

        print 'DB: {}'.format(db)

        cls = cli[db].collection_names()
	lcl = [c for c in cls if cli[db][c].find().count() > MIN_SIZE]
        t1 = sum([cli[db][c].find().count() for c in cls])

	if db == DB_NAME:
            print 'COLLECTIONS: {} \nCOLLECTIONS > {}: {}\nTOTAL ENTRIES: {}'.format(len(cls), MIN_SIZE, len(lcl), t1)
	else:
	    print 'COLLECTIONS: {} \nTOTAL ENTRIES: {}'.format(len(cls), t1)           
print 25*'='
