import io
import os
import sys
import gzip
import ujson
import numpy as np
from tqdm import tqdm
from config import DB_NAME, PRUNED_KEYS, MIN_SIZE
from itertools import groupby
from pymongo import MongoClient
from datetime import datetime, timedelta
from params_encoder import ParamsEncoder
from scipy.sparse import coo_matrix, hstack, vstack
from config import DB_NAME, PRUNED_KEYS, PROG, GROUP_INTERVAL

# <low_quality_mode>
def gps_to_datetime(gpstime):
    return datetime.utcfromtimestamp(gpstime/1000000)


def connect_to_mongo(port, host='localhost'):
    client = MongoClient(host, port, serverSelectionTimeoutMS = 5)
    try:
        _ = client.database_names()
        return client
    except:
        print('Could not establish a connection to the database, exiting.')
        raise


def build_encoding(keys):
    return {k:i for i,k in enumerate(sorted(keys))}


def get_all_keys(dicts):
    keys = set()
    for d in dicts:
        keys.update(d.keys())
    return keys


def load_from_col(col, viz=PROG):
    if viz:
        bar = tqdm(total=col.count())
        B = []
        for ob in col.find():
            B.append(ob)
            bar.update(1)
        bar.close()
        return B
    else:
        return list(col.find())

    
def prepare_run_numbers(runs):
    return [int(str(r).split('_')[1]) if 'run_' in str(r) else int(r) for r in np.atleast_1d(runs)]


def get_key_by_datetime(time, interval):
    k = time + timedelta(minutes=-(time.minute % interval)) 
    return datetime(k.year, k.month, k.day, k.hour, k.minute, 0)


def get_key(gps_time, interval=GROUP_INTERVAL):
    """
    Group by GROUP_INTERVAL minutes. TODO(kazeevn) test.
    """
    return get_key_by_datetime(gps_to_datetime(gps_time), interval)


def load_runs_data(cli, runs):
    k = set()
    run_names = cli[DB_NAME].collection_names()

    runs = prepare_run_numbers(runs)
    RDAT = {}
    
    for run in runs:
        col_name = 'run_{}'.format(run)

        if col_name in run_names:
            dat_col = cli[DB_NAME][col_name]
            
            RDAT[col_name] = load_from_col(dat_col)
            rkeys = get_all_keys(RDAT[col_name])
            k.update(rkeys)

        else:
            print('Collection {} not found in DB {}'.format(
                col_name, DB_NAME))

    encoder = ParamsEncoder()
    encoder.load_from_dict(build_encoding(k))
    return RDAT, encoder
    

def runs_to_sparse(cli, runs):
    runs = prepare_run_numbers(runs)
    RDAT, encoder = load_runs_data(cli, runs)
    
    D = []
    for run in runs:
        col_name = 'run_{}'.format(run)
        if col_name in RDAT:
            D.append(dict_list_to_sparse(RDAT[col_name], encoder))
        else:
            D.append(dict_list_to_sparse(load_from_col(cli[DB_NAME][col_name]), encoder))

    return D, encoder

        
def dict_list_to_sparse(col, enc):
    values_list = []
    event_indices = []
    param_indices = []
    selected_events = 0

    for event in col:
        event_dict = enc.encode_event(event)
        for param, value in event_dict.items():
            if not value:
                continue
            values_list.append(value)
            param_indices.append(param)
            event_indices.append(selected_events)
        selected_events += 1

    return coo_matrix((np.array(values_list, dtype=np.float), 
                      (np.array(event_indices, dtype=np.int),
                       np.array(param_indices, dtype=np.int))), 
                       shape=(selected_events, len(enc))).tocsc()


def run_to_sparse_partitions(cli, run, interval=GROUP_INTERVAL):
    D, E = load_runs_data(cli, run)
    data = next(iter(D.values())) # We only load a single run here
    data = sorted(data, key=lambda k: k['gpsTime'])
    tst = [gps_to_datetime(d['gpsTime']) for d in data]
    g = groupby(data, key=lambda event: get_key(event['gpsTime'], interval=interval))
    return {key: dict_list_to_sparse(list(items), E) for key, items in g}, tst
# </low_quality_mode>


def run_number_to_collection_name(run_number):
    return "run_%d" % run_number


def collection_name_to_run_number(collection_name):
    return int(collection_name.split("_")[1])


def get_runs_with_data(cli, min_size=MIN_SIZE):
    """
    Returns the run numbers for all runs having at least min_size
    events in the database.
    """
    db = cli[DB_NAME]
    return list(map(collection_name_to_run_number,
                    filter(lambda collection: db[collection].count() > min_size,
                           db.collection_names())))
    
