import requests
import json
from operator import itemgetter
from functools import partial
from itertools import chain
import datetime

from run_db import RUN_DB_SINGLE_RUN_QUERY
RUN_DB_SEARCH_QUERY = r"https://lbrundb.cern.ch/api/search/?partition=LHCb"


def get_runs_chain(run_id, count, key):
    """Gets a sorted chain of runs of length count.
    A chain is a list of run such as each susequent is referenced by
    the <key> field of the preceding. Example keys: prev_runid, next_runid.
    The result doesn't include the starting run_id."""
    results = []
    current_run = run_id
    for step in range(count):
        next_run_data = json.loads(requests.get(
            RUN_DB_SINGLE_RUN_QUERY.format(current_run)).text)
        if key in next_run_data:
            current_run = next_run_data[key]
            results.append(current_run)
        else:
            break
    return sorted(results)
    

def get_problems(started_at):
    return json.loads(requests.get(
        r"http://lbproblems.cern.ch/api/activity/?started_at=%s" %
        started_at.strftime(r"%Y-%m-%d")).text)

def get_rundb_search_params(problem):
    params = {"starttime": problem["started_at"]}
    if "ended_at" in problem:
        params["endtime"] = problem["ended_at"]
    return params
    
def get_runs_for_problems(started_at):
    PROBLEM_REQURED_KEYS = ("started_at", )
    def check_keys(problem):
        for key in PROBLEM_REQURED_KEYS:
            if key not in problem:
                return False
        return True
    problems = list(filter(check_keys, get_problems(started_at)))
    # For each problem we want to test the detection ability.
    # To that end we want 3 preceding runs,
    # all the problematic runs and 3 subsequent runs

    # affected_runs is a list of runs affected by a particular problem
    affected_runs = list(map(lambda problem:
                                    sorted(map(itemgetter("runid"),
        json.loads(requests.get(RUN_DB_SEARCH_QUERY, params=
                                get_rundb_search_params(problem)
        ).text)['runs'])), problems))
    preceding_runs = list(map(
        lambda runs: get_runs_chain(min(runs), 3, "prev_runid") if
        len(runs) > 0 else [], affected_runs))
    subsequent_runs = list(map(
        lambda runs: get_runs_chain(max(runs), 3, "next_runid") if
        len(runs) > 0 else [], affected_runs))
    return list(zip(
        problems, preceding_runs, affected_runs, subsequent_runs))

    
def save_recent_problematic_runs():
    start = datetime.datetime(2015, 1, 1)
    runs = get_runs_for_problems(start)
    with open("problems_since_2015.json", "w") as problems_io:
        json.dump(runs, problems_io)
    

if __name__ == "__main__":
    save_recent_problematic_runs()
